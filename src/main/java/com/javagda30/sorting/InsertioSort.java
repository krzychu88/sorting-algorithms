package com.javagda30.sorting;

public class InsertioSort {
    public static void sort(int[] tablica) {

        int licznik =0;
        // zaczynamy od dwoch elementów
        for (int i = 1; i < tablica.length; i++){
            int elementAktualnieNiePorownywany = tablica[i];

            int indeksPorownywany = i-1;
            while (indeksPorownywany >= 0){
                licznik++;
                if (elementAktualnieNiePorownywany < tablica[indeksPorownywany]){
                    //swap
                    int tmp = tablica[indeksPorownywany];
                    tablica[indeksPorownywany] = tablica[indeksPorownywany +1];
                    tablica[indeksPorownywany +1] = tmp;
                } else {
                    break;
                }

                indeksPorownywany--;
            }
//            tablica[indeksPorownywany] = elementAktualnieNiePorownywany;
        }
        System.out.println("Licznik instrukcji: " + licznik);

    }
}
