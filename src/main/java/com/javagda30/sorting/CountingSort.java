package com.javagda30.sorting;

public class CountingSort {
    public static void sort(int tablica[]) {
        // nasze sortowanie zaklada tylko wartosci doddatnie
        // mjsimy odnalzezc minimumi maksimum

        int max = tablica[0];
        for (int i=1; i< tablica.length; i++){
            if (max  < tablica[i]){
                max = tablica[i];
            }
        }
        System.out.println("Maksimum: " + max);

        int[] tablicaZliczen = new int[max + 1];
        for (int i = 0; i < tablica.length; i++){
            tablicaZliczen[tablica[i]]++;
        }
        int indeksWstawiania =0;
        for (int j = 0; j <tablicaZliczen.length; j++){     //idz przez wszystkie wartosci
            //for(int i=0; i < tablicaZliczen[j]; i++){}
            while (tablicaZliczen[j]-- >0){
                tablica[indeksWstawiania++] = j;
            }
        }
    }
}
