package com.javagda30.sorting;


public class BubbleSort {


//        procedure bubbleSort( A : lista elementów do posortowania )
//        n = liczba_elementów(A)
//        do
//            for (i = 0; i < n-1; i++) do:
//        if A[i] > A[i+1] then
//        swap(A[i], A[i+1])
//        end if
//        end for
//        n = n-1
//        while n > 1
//        end procedure

    public static void sort (int[] tablica) {
        int n = tablica.length;
        int licznik = 0;
        do {
            for (int i = 0; i < n - 1; i++) {
                licznik++;
                if (tablica[i] > tablica[i + 1]) {
                    int tymczasowa = tablica[i];
                    tablica[i] = tablica[i + 1];
                    tablica[i + 1] = tymczasowa;
                }
            }
            n =n-1;
        } while (n>1);
        System.out.println("Licznik operacji: " + licznik);

    }



}