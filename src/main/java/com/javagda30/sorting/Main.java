package com.javagda30.sorting;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {

        int[] tablicaPes = new int[] {10, 9, 8, 7, 6 ,5, 4, 3, 2, 1};
        int[] tablicaOpt = new int[] {1, 2, 3, 4, 5 ,6, 7, 8, 9, 10};
        int[] tablicaRand = new int[] {10, 6, 5, 9, 7 ,2, 3, 4, 1, 8};


        System.out.println("Pesymistycznie: ");
//        BubbleSort.sort (tablicaPes);
//        InsertioSort.sort (tablicaPes);
        CountingSort.sort (tablicaPes);
        System.out.println(Arrays.toString(tablicaPes));

        System.out.println("Optymistycznie: ");
//        BubbleSort.sort (tablicaOpt);
//        InsertioSort.sort (tablicaOpt);
        CountingSort.sort (tablicaOpt);
        System.out.println(Arrays.toString(tablicaOpt));

        System.out.println("Losowo: ");
//        BubbleSort.sort (tablicaRand);
//        InsertioSort.sort (tablicaRand);
        CountingSort.sort (tablicaRand);
        System.out.println(Arrays.toString(tablicaRand));
    }

}
